#ifndef _HUM_SAM3S_QFP64_H
#define _HUM_SAM3S_QFP64_H



//---- PIOA --------------------------------------------------------------------
#define HUM_PIOA_0    0 // Pin 48
#define HUM_PIOA_1    1 // Pin 47
#define HUM_PIOA_2    0 // Pin 44
#define HUM_PIOA_3    1 // Pin 43
#define HUM_PIOA_4    1 // Pin 36
#define HUM_PIOA_5    0 // Pin 35
#define HUM_PIOA_6    1 // Pin 34
#define HUM_PIOA_7    1 // Pin 32
#define HUM_PIOA_8    0 // Pin 31
#define HUM_PIOA_9    1 // Pin 30
#define HUM_PIOA_10   0 // Pin 29
#define HUM_PIOA_11   1 // Pin 28
#define HUM_PIOA_12   0 // Pin 27
#define HUM_PIOA_13   1 // Pin 22
#define HUM_PIOA_14   0 // Pin 21
#define HUM_PIOA_15   1 // Pin 20
#define HUM_PIOA_16   0 // Pin 19
#define HUM_PIOA_17   0 // Pin 9
#define HUM_PIOA_18   1 // Pin 10
#define HUM_PIOA_19   0 // Pin 13
#define HUM_PIOA_20   1 // Pin 16
#define HUM_PIOA_21   0 // Pin 11
#define HUM_PIOA_22   1 // Pin 14
#define HUM_PIOA_23   0 // Pin 15
#define HUM_PIOA_24   0 // Pin 23
#define HUM_PIOA_25   0 // Pin 25
#define HUM_PIOA_26   1 // Pin 26
#define HUM_PIOA_27   0 // Pin 37
#define HUM_PIOA_28   1 // Pin 38
#define HUM_PIOA_29   1 // Pin 41
#define HUM_PIOA_30   0 // Pin 42
#define HUM_PIOA_31   0 // Pin 52

//---- PIOB --------------------------------------------------------------------
#define HUM_PIOB_0    1 // Pin 3
#define HUM_PIOB_1    0 // Pin 4
#define HUM_PIOB_2    1 // Pin 5
#define HUM_PIOB_3    0 // Pin 6
#define HUM_PIOB_4    0 // Pin 33
#define HUM_PIOB_5    1 // Pin 49
#define HUM_PIOB_6    1 // Pin 51
#define HUM_PIOB_7    1 // Pin 53
#define HUM_PIOB_8    1 // Pin 61
#define HUM_PIOB_9    0 // Pin 62
#define HUM_PIOB_10   1 // Pin 56
#define HUM_PIOB_11   0 // Pin 57
#define HUM_PIOB_12   0 // Pin 55
#define HUM_PIOB_13   0 // Pin 59
#define HUM_PIOB_14   1 // Pin 63
// Last PIO = PB14


//---- PIOC --------------------------------------------------------------------
// Not implemented

//---- PIOD --------------------------------------------------------------------
// Not implemented

//---- PIOE --------------------------------------------------------------------
// Not implemented


//------------------------------------------------------------------------------


#endif //_HUM_SAM3S_QFP64_H
